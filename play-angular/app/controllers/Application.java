package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.Index;

/**
 * Created by dayj on 7/8/15.
 */
public class Application extends Controller {

    public Result index() {
        return ok(Index.render());
    }


}
