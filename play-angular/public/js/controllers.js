/*global define */

'use strict';

define(function() {

/* Controllers */

var controllers = {};

controllers.MyCtrl1 = function() {};
controllers.MyCtrl1.$inject = [];

controllers.MyCtrl2 = function() {};
controllers.MyCtrl2.$inject = [];

controllers.CSSController = function() {};
controllers.CSSController.$inject = [];

controllers.AngularController = function($scope) {
    $scope.placeholder = 'Still need to type one.';
};

controllers.AngularController.$inject = ['$scope'];

return controllers;

});